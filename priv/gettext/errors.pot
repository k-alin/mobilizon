## This file is a PO Template file.
##
## `msgid`s here are often extracted from source code.
## Add new translations manually only if they're dynamic
## translations that can't be statically extracted.
##
## Run `mix gettext.extract` to bring this file up to
## date. Leave `msgstr`s empty as changing them here as no
## effect: edit them in PO (`.po`) files instead.
## From Ecto.Changeset.cast/4
#: lib/mobilizon/discussions/discussion.ex:69
msgid "can't be blank"
msgstr ""

## From Ecto.Changeset.unique_constraint/3
msgid "has already been taken"
msgstr ""

## From Ecto.Changeset.put_change/3
msgid "is invalid"
msgstr ""

## From Ecto.Changeset.validate_acceptance/3
msgid "must be accepted"
msgstr ""

## From Ecto.Changeset.validate_format/3
msgid "has invalid format"
msgstr ""

## From Ecto.Changeset.validate_subset/3
msgid "has an invalid entry"
msgstr ""

## From Ecto.Changeset.validate_exclusion/3
msgid "is reserved"
msgstr ""

## From Ecto.Changeset.validate_confirmation/3
msgid "does not match confirmation"
msgstr ""

## From Ecto.Changeset.no_assoc_constraint/3
msgid "is still associated with this entry"
msgstr ""

msgid "are still associated with this entry"
msgstr ""

## From Ecto.Changeset.validate_length/3
msgid "should be %{count} character(s)"
msgid_plural "should be %{count} character(s)"
msgstr[0] ""
msgstr[1] ""

msgid "should have %{count} item(s)"
msgid_plural "should have %{count} item(s)"
msgstr[0] ""
msgstr[1] ""

msgid "should be at least %{count} character(s)"
msgid_plural "should be at least %{count} character(s)"
msgstr[0] ""
msgstr[1] ""

msgid "should have at least %{count} item(s)"
msgid_plural "should have at least %{count} item(s)"
msgstr[0] ""
msgstr[1] ""

msgid "should be at most %{count} character(s)"
msgid_plural "should be at most %{count} character(s)"
msgstr[0] ""
msgstr[1] ""

msgid "should have at most %{count} item(s)"
msgid_plural "should have at most %{count} item(s)"
msgstr[0] ""
msgstr[1] ""

## From Ecto.Changeset.validate_number/3
msgid "must be less than %{number}"
msgstr ""

msgid "must be greater than %{number}"
msgstr ""

msgid "must be less than or equal to %{number}"
msgstr ""

msgid "must be greater than or equal to %{number}"
msgstr ""

msgid "must be equal to %{number}"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:107
msgid "Cannot refresh the token"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:252
msgid "Current profile is not a member of this group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:256
msgid "Current profile is not an administrator of the selected group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:612
msgid "Error while saving user settings"
msgstr ""

#, elixir-format
#: lib/graphql/error.ex:99
#: lib/graphql/resolvers/group.ex:249
#: lib/graphql/resolvers/group.ex:281
#: lib/graphql/resolvers/group.ex:318
#: lib/graphql/resolvers/group.ex:349
#: lib/graphql/resolvers/group.ex:398
#: lib/graphql/resolvers/member.ex:81
msgid "Group not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:78
#: lib/graphql/resolvers/group.ex:82
msgid "Group with ID %{id} not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:85
msgid "Impossible to authenticate, either your email or password are invalid."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:315
msgid "Member not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/actor.ex:94
msgid "No profile found for the moderator user"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:273
msgid "No user to validate with this email was found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/person.ex:314
#: lib/graphql/resolvers/user.ex:298
msgid "No user with this email was found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:28
#: lib/graphql/resolvers/participant.ex:32
#: lib/graphql/resolvers/participant.ex:210
#: lib/graphql/resolvers/person.ex:236
#: lib/graphql/resolvers/person.ex:353
#: lib/graphql/resolvers/person.ex:380
#: lib/graphql/resolvers/person.ex:397
#: lib/graphql/resolvers/person.ex:425
#: lib/graphql/resolvers/person.ex:440
msgid "Profile is not owned by authenticated user"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:160
msgid "Registrations are not open"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:429
msgid "The current password is invalid"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/admin.ex:341
#: lib/graphql/resolvers/user.ex:472
msgid "The new email doesn't seem to be valid"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/admin.ex:344
#: lib/graphql/resolvers/user.ex:475
msgid "The new email must be different"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:432
msgid "The new password must be different"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:479
#: lib/graphql/resolvers/user.ex:541
#: lib/graphql/resolvers/user.ex:544
msgid "The password provided is invalid"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:436
msgid "The password you have chosen is too short. Please make sure your password contains at least 6 characters."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:294
msgid "This user can't reset their password"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:81
msgid "This user has been disabled"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:252
#: lib/graphql/resolvers/user.ex:257
msgid "Unable to validate user"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:522
msgid "User already disabled"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:587
msgid "User requested is not logged-in"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:287
msgid "You are already a member of this group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:322
msgid "You can't leave this group because you are the only administrator"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:284
msgid "You cannot join this group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:112
msgid "You may not list groups unless moderator."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:487
msgid "You need to be logged-in to change your email"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:444
msgid "You need to be logged-in to change your password"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:261
msgid "You need to be logged-in to delete a group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:549
msgid "You need to be logged-in to delete your account"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:292
msgid "You need to be logged-in to join a group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:327
msgid "You need to be logged-in to leave a group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:225
msgid "You need to be logged-in to update a group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:112
msgid "You need to have an existing token to get a refresh token"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:276
#: lib/graphql/resolvers/user.ex:301
msgid "You requested again a confirmation email too soon"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:163
msgid "Your email is not on the allowlist"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/actor.ex:100
msgid "Error while performing background task"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/actor.ex:32
msgid "No profile found with this ID"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/actor.ex:61
#: lib/graphql/resolvers/actor.ex:97
msgid "No remote profile found with this ID"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/actor.ex:72
msgid "Only moderators and administrators can suspend a profile"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/actor.ex:105
msgid "Only moderators and administrators can unsuspend a profile"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/actor.ex:29
msgid "Only remote profiles may be refreshed"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/actor.ex:64
msgid "Profile already suspended"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:96
msgid "A valid email is required by your instance"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:90
#: lib/graphql/resolvers/participant.ex:143
msgid "Anonymous participation is not enabled"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/person.ex:210
msgid "Cannot remove the last administrator of a group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/person.ex:207
msgid "Cannot remove the last identity of a user"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/comment.ex:126
msgid "Comment is already deleted"
msgstr ""

#, elixir-format
#: lib/graphql/error.ex:101
#: lib/graphql/resolvers/discussion.ex:69
msgid "Discussion not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/report.ex:71
#: lib/graphql/resolvers/report.ex:90
msgid "Error while saving report"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/report.ex:110
msgid "Error while updating report"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:131
msgid "Event id not found"
msgstr ""

#, elixir-format
#: lib/graphql/error.ex:98
#: lib/graphql/resolvers/event.ex:360
#: lib/graphql/resolvers/event.ex:412
msgid "Event not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:87
#: lib/graphql/resolvers/participant.ex:128
#: lib/graphql/resolvers/participant.ex:155
#: lib/graphql/resolvers/participant.ex:336
msgid "Event with this ID %{id} doesn't exist"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:103
msgid "Internal Error"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/discussion.ex:219
msgid "No discussion with ID %{id}"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/todos.ex:80
#: lib/graphql/resolvers/todos.ex:107
#: lib/graphql/resolvers/todos.ex:179
#: lib/graphql/resolvers/todos.ex:208
#: lib/graphql/resolvers/todos.ex:237
msgid "No profile found for user"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:64
msgid "No such feed token"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:259
msgid "Participant already has role %{role}"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:187
#: lib/graphql/resolvers/participant.ex:220
#: lib/graphql/resolvers/participant.ex:263
msgid "Participant not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/person.ex:32
msgid "Person with ID %{id} not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/person.ex:56
msgid "Person with username %{username} not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/post.ex:169
#: lib/graphql/resolvers/post.ex:203
msgid "Post ID is not a valid ID"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/post.ex:172
#: lib/graphql/resolvers/post.ex:206
msgid "Post doesn't exist"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:84
msgid "Profile invited doesn't exist"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:93
#: lib/graphql/resolvers/member.ex:97
msgid "Profile is already a member of this group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/post.ex:133
#: lib/graphql/resolvers/post.ex:175
#: lib/graphql/resolvers/post.ex:209
#: lib/graphql/resolvers/resource.ex:90
#: lib/graphql/resolvers/resource.ex:132
#: lib/graphql/resolvers/resource.ex:165
#: lib/graphql/resolvers/resource.ex:199
#: lib/graphql/resolvers/todos.ex:58
#: lib/graphql/resolvers/todos.ex:83
#: lib/graphql/resolvers/todos.ex:110
#: lib/graphql/resolvers/todos.ex:182
#: lib/graphql/resolvers/todos.ex:214
#: lib/graphql/resolvers/todos.ex:246
msgid "Profile is not member of group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/actor.ex:67
#: lib/graphql/resolvers/person.ex:233
msgid "Profile not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/report.ex:48
msgid "Report not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/resource.ex:169
#: lib/graphql/resolvers/resource.ex:196
msgid "Resource doesn't exist"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:124
msgid "The event has already reached its maximum capacity"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:282
msgid "This token is invalid"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/todos.ex:176
#: lib/graphql/resolvers/todos.ex:243
msgid "Todo doesn't exist"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/todos.ex:77
#: lib/graphql/resolvers/todos.ex:211
#: lib/graphql/resolvers/todos.ex:240
msgid "Todo list doesn't exist"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:73
msgid "Token does not exist"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:67
#: lib/graphql/resolvers/feed_token.ex:70
msgid "Token is not a valid UUID"
msgstr ""

#, elixir-format
#: lib/graphql/error.ex:96
msgid "User not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/person.ex:310
msgid "You already have a profile for this user"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:134
msgid "You are already a participant of this event"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:87
msgid "You are not a member of this group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:157
#: lib/graphql/resolvers/member.ex:173
#: lib/graphql/resolvers/member.ex:188
msgid "You are not a moderator or admin for this group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/comment.ex:59
msgid "You are not allowed to create a comment if not connected"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:41
msgid "You are not allowed to create a feed token if not connected"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/comment.ex:134
msgid "You are not allowed to delete a comment if not connected"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:82
msgid "You are not allowed to delete a feed token if not connected"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/comment.ex:93
msgid "You are not allowed to update a comment if not connected"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:181
#: lib/graphql/resolvers/participant.ex:214
msgid "You can't leave event because you're the only event creator participant"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:192
msgid "You can't set yourself to a lower member role for this group because you are the only administrator"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/comment.ex:122
msgid "You cannot delete this comment"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:408
msgid "You cannot delete this event"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:90
msgid "You cannot invite to this group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/feed_token.ex:76
msgid "You don't have permission to delete this token"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/admin.ex:56
msgid "You need to be logged-in and a moderator to list action logs"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/report.ex:36
msgid "You need to be logged-in and a moderator to list reports"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/report.ex:115
msgid "You need to be logged-in and a moderator to update a report"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/report.ex:53
msgid "You need to be logged-in and a moderator to view a report"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/admin.ex:257
msgid "You need to be logged-in and an administrator to access admin settings"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/admin.ex:241
msgid "You need to be logged-in and an administrator to access dashboard statistics"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/admin.ex:283
msgid "You need to be logged-in and an administrator to save admin settings"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/discussion.ex:84
msgid "You need to be logged-in to access discussions"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/resource.ex:96
msgid "You need to be logged-in to access resources"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:318
msgid "You need to be logged-in to create events"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/post.ex:141
msgid "You need to be logged-in to create posts"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/report.ex:87
msgid "You need to be logged-in to create reports"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/resource.ex:137
msgid "You need to be logged-in to create resources"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:417
msgid "You need to be logged-in to delete an event"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/post.ex:214
msgid "You need to be logged-in to delete posts"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/resource.ex:204
msgid "You need to be logged-in to delete resources"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:108
msgid "You need to be logged-in to join an event"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:225
msgid "You need to be logged-in to leave an event"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:374
msgid "You need to be logged-in to update an event"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/post.ex:180
msgid "You need to be logged-in to update posts"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/resource.ex:174
msgid "You need to be logged-in to update resources"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/resource.ex:233
msgid "You need to be logged-in to view a resource preview"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/resource.ex:129
msgid "Parent resource doesn't belong to this group"
msgstr ""

#, elixir-format
#: lib/mobilizon/users/user.ex:114
msgid "The chosen password is too short."
msgstr ""

#, elixir-format
#: lib/mobilizon/users/user.ex:142
msgid "The registration token is already in use, this looks like an issue on our side."
msgstr ""

#, elixir-format
#: lib/mobilizon/users/user.ex:108
msgid "This email is already used."
msgstr ""

#, elixir-format
#: lib/graphql/error.ex:97
msgid "Post not found"
msgstr ""

#, elixir-format
#: lib/graphql/error.ex:84
msgid "Invalid arguments passed"
msgstr ""

#, elixir-format
#: lib/graphql/error.ex:90
msgid "Invalid credentials"
msgstr ""

#, elixir-format
#: lib/graphql/error.ex:88
msgid "Reset your password to login"
msgstr ""

#, elixir-format
#: lib/graphql/error.ex:95
#: lib/graphql/error.ex:100
msgid "Resource not found"
msgstr ""

#, elixir-format
#: lib/graphql/error.ex:102
msgid "Something went wrong"
msgstr ""

#, elixir-format
#: lib/graphql/error.ex:83
msgid "Unknown Resource"
msgstr ""

#, elixir-format
#: lib/graphql/error.ex:93
msgid "You don't have permission to do this"
msgstr ""

#, elixir-format
#: lib/graphql/error.ex:85
msgid "You need to be logged in"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:118
msgid "You can't accept this invitation with this profile."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:139
msgid "You can't reject this invitation with this profile."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/media.ex:71
msgid "File doesn't have an allowed MIME type."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:220
msgid "Profile is not administrator for the group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:363
msgid "You can't edit this event."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:366
msgid "You can't attribute this event to this profile."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:142
msgid "This invitation doesn't exist."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:217
msgid "This member already has been rejected."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:241
msgid "You don't have the right to remove this member."
msgstr ""

#, elixir-format
#: lib/mobilizon/actors/actor.ex:351
msgid "This username is already taken."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/discussion.ex:81
msgid "You must provide either an ID or a slug to access a discussion"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:313
msgid "Organizer profile is not owned by the user"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:93
msgid "Profile ID provided is not the anonymous profile one"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:161
#: lib/graphql/resolvers/group.ex:203
#: lib/graphql/resolvers/person.ex:148
#: lib/graphql/resolvers/person.ex:182
#: lib/graphql/resolvers/person.ex:304
msgid "The provided picture is too heavy"
msgstr ""

#, elixir-format
#: lib/web/views/utils.ex:34
msgid "Index file not found. You need to recompile the front-end."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/resource.ex:126
msgid "Error while creating resource"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:505
msgid "Invalid activation token"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/resource.ex:223
msgid "Unable to fetch resource details from this URL."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:164
#: lib/graphql/resolvers/participant.ex:253
#: lib/graphql/resolvers/participant.ex:328
msgid "Provided profile doesn't have moderator permissions on this event"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:299
msgid "Organizer profile doesn't have permission to create an event on behalf of this group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:354
msgid "This profile doesn't have permission to update an event on behalf of this group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:167
msgid "Your e-mail has been denied registration or uses a disallowed e-mail provider"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/comment.ex:129
msgid "Comment not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/discussion.ex:123
msgid "Error while creating a discussion"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:626
msgid "Error while updating locale"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/person.ex:307
msgid "Error while uploading pictures"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:190
msgid "Failed to leave the event"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:216
msgid "Failed to update the group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/admin.ex:338
#: lib/graphql/resolvers/user.ex:469
msgid "Failed to update user email"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:501
msgid "Failed to validate user email"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:146
msgid "The anonymous actor ID is invalid"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/resource.ex:162
msgid "Unknown error while updating resource"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/comment.ex:84
msgid "You are not the comment creator"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:426
msgid "You cannot change your password."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:321
msgid "Format not supported"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:305
msgid "A dependency needed to export to %{format} is not installed"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/participant.ex:313
msgid "An error occured while saving export"
msgstr ""

#, elixir-format
#: lib/web/controllers/export_controller.ex:30
msgid "Export to format %{format} is not enabled on this instance"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:167
msgid "Only admins can create groups"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:306
msgid "Only groups can create events"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/event.ex:292
msgid "Unknown error while creating event"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:482
msgid "User cannot change email"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:371
msgid "Follow does not match your account"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:375
msgid "Follow not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:347
msgid "Profile with username %{username} not found"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:342
msgid "This profile does not belong to you"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:345
msgid "You are already following this group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:354
msgid "You need to be logged-in to follow a group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:403
msgid "You need to be logged-in to unfollow a group"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/group.ex:380
msgid "You need to be logged-in to update a group follow"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:210
msgid "This member does not exist"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:234
msgid "You don't have the role needed to remove this member."
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/member.ex:252
msgid "You must be logged-in to remove a member"
msgstr ""

#, elixir-format
#: lib/graphql/resolvers/user.ex:157
msgid "Your email seems to be using an invalid format"
msgstr ""

#, elixir-format, ex-autogen
#: lib/graphql/resolvers/admin.ex:387
msgid "Can't confirm an already confirmed user"
msgstr ""

#, elixir-format, ex-autogen
#: lib/graphql/resolvers/admin.ex:391
msgid "Deconfirming users is not supported"
msgstr ""

#, elixir-format, ex-autogen
#: lib/graphql/resolvers/admin.ex:363
msgid "The new role must be different"
msgstr ""

#, elixir-format, ex-autogen
#: lib/graphql/resolvers/admin.ex:314
msgid "You need to be logged-in and an administrator to edit an user's details"
msgstr ""
